function createCard(name, description, pictureUrl, start, end, subtitle) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer"> ${start} - ${end}</div>
        </div>
      </div>
    `;
  }


window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

try {
    const response = await fetch(url);
    if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error("Response not ok");
    } else {
        const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();

          const start = new Date(details.conference.starts).toDateString();
          const end = new Date(details.conference.ends).toDateString();
          const subtitle = details.conference.location.name
          
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(title, description, pictureUrl, start, end, subtitle);
          const column = document.querySelector('.col');
          column.innerHTML += html;
        }
    }
    }
} catch (e) {
    // Figure out what to do if an error is raised
    console.error(e);
  }
});
