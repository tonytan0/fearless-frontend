window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    if(response.ok) {
        const conferenceData = await response.json();
        const conferenceTag = document.getElementById('conference')
        for (const conference of conferenceData.conferences ){
            const conferenceName = conference.name;
            const conferenceHref = conference.href;
            const option = document.createElement('option')
            option.innerHTML = conferenceName;
            option.value = conferenceHref;
            conferenceTag.appendChild(option)
        }
        const formTag = document.getElementById("create-presentation-form")
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            

            const conferenceId = conferenceTag.options[conferenceTag.selectedIndex].value;
            const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },

            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation)
            }
        })
    }

});
